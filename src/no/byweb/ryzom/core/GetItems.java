package no.byweb.ryzom.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.util.Log;

public class GetItems extends AsyncTask<String, Integer, String> {
	static final String TAG = "GetItems";

	public static final int LOC_UNKNOWN = 0;
	public static final int LOC_INVENTORY = 1;
	public static final int LOC_ROOM = 2;
	public static final int LOC_PET1 = 3;
	public static final int LOC_PET2 = 4;
	public static final int LOC_PET3 = 5;
	public static final int LOC_PET4 = 6;

	protected List<Item> allItems = new ArrayList<Item>();

	private XmlPullParserFactory factory;
	private FileInputStream fis = null;
	private String fileContent = null;
	protected DbRyzomExtra myDbHelper;

	private String xmlFile = null;
	private String[] xmlFiles = null;
	private String locationName = "Unknown";

	private String index = null;

	private String id = null;
	private String name = null;
	private String sheet = null;
	private String sheetId = null;
	private String color = null;
	private String hpbuff = null;
	private String focusbuff = null;
	private String sapbuff = null;
	private String stabuff = null;
	private String stack = null;
	private String quality = null;
	private String locked = null;
	private String hp = null;
	private String sort = null;

	private boolean inBag = false;
	private boolean inRoom = false;
	private boolean inPets = false;
	private boolean inPet1 = false;
	private boolean inPet1inventory = false;
	private boolean inPet2 = false;
	private boolean inPet2inventory = false;
	private boolean inPet3 = false;
	private boolean inPet3inventory = false;
	private boolean inPet4 = false;
	private boolean inPet4inventory = false;
	private boolean setInItem = false;
	private boolean inItem = false;
	private int itemLevel;

	private Context context;

	public GetItems(String xmlFile, Context context) {
		this.context = context;
		this.xmlFile = xmlFile;
	}

	public GetItems(String[] xmlFiles, Context context) {
		this.context = context;
		this.xmlFiles = xmlFiles;
	}

	@Override
	protected String doInBackground(String... args) {

		myDbHelper = new DbRyzomExtra(context);
		myDbHelper.openDataBase();

		if (xmlFile != null) {
			getItems(xmlFile);
		} else {
			for (int i = 0; i < xmlFiles.length; i++) {
				if (xmlFiles[i].length() == 45) {
					getItems(xmlFiles[i]);
				}
			}
		}
		myDbHelper.close();

		return null;
	}

	protected void getItems(String xmlFile) {
		if (xmlFile != null) {
			Log.d(TAG, "Adding from: " + xmlFile);
			if (xmlFile.startsWith("g")) {
				try {
					fis = context.openFileInput(xmlFile);
					byte[] buffer = new byte[fis.available()];
					while (fis.read(buffer) != -1) {
						fileContent = new String(buffer);
					}
					factory = XmlPullParserFactory.newInstance();
					factory.setNamespaceAware(true);
					XmlPullParser xpp = factory.newPullParser();
					xpp.setInput(new StringReader(fileContent));
					int eventType = xpp.getEventType();
					String tag = null;

					while (eventType != XmlPullParser.END_DOCUMENT) {
						String tagName = xpp.getName();
						switch (eventType) {
						case XmlPullParser.START_TAG:
							tag = tagName;

							if (xpp.getDepth() == 3) {
								if (tag.equals("room")) {
									inRoom = true;
								}
							} else if ((xpp.getDepth() == 4) && (tag.equals("item"))) {
								if (inRoom == true) {
									setInItem = true;
								}
							}

							if (setInItem == true) {
								inItem = true;
								itemLevel = xpp.getDepth();
								id = xpp.getAttributeValue(null, "id");
								if (id == null) {
									Log.d(TAG, "Aborting! Could not find item id.");
									inItem = false;
								}
								setInItem = false;
							}

							break;
						case XmlPullParser.TEXT:
							if ((inItem == true) && (xpp.getDepth() == itemLevel + 1)) {
								if (tag.equals("stack")) {
									stack = xpp.getText();
								} else if (tag.equals("sheet")) {
									sheet = xpp.getText();
									if ((sheet != null) && (sheet.length() > 7)) {
										sheetId = sheet.substring(0, sheet.length() - 6);
									}
								} else if (tag.equals("quality")) {
									quality = xpp.getText();
								} else if (tag.equals("locked")) {
									locked = xpp.getText();
								} else if (tag.equals("hp")) {
									hp = xpp.getText();
								}
							} else if ((inItem == true) && (xpp.getDepth() == itemLevel + 2)) {
								if (tag.equals("color")) {
									color = xpp.getText();
								} else if (tag.equals("hpbuff")) {
									hpbuff = xpp.getText();
								} else if (tag.equals("focusbuff")) {
									focusbuff = xpp.getText();
								} else if (tag.equals("sapbuff")) {
									sapbuff = xpp.getText();
								} else if (tag.equals("stabuff")) {
									stabuff = xpp.getText();
								}
							} else if ((xpp.getDepth() == 3) && (tag.equals("name"))) {
								locationName = xpp.getText();
							}
							break;
						case XmlPullParser.END_TAG:
							tag = tagName;

							if ((inItem == true) && (tag.equals("item")) && (xpp.getDepth() == itemLevel)) {
								if ((sheet != null) && (sheet.length() > 7)) {
									sort = "";
									Cursor cursor = null;
									try {
										cursor = myDbHelper.getReadableDatabase().query("words_en_item",
												new String[] { "name" }, "_id = '" + sheetId + "'", null, null, null,
												null);
										if (cursor.moveToFirst()) {
											if (cursor.getString(0) != null) {
												name = cursor.getString(0);
											}
										}
										cursor = myDbHelper.getReadableDatabase().query(
												"items",
												new String[] { "type", "item_type", "mpft", "race", "icon_main",
														"icon_back", "txt" }, "_id = '" + sheetId + "'", null, null,
												null, null);
										if (cursor.moveToFirst()) {
											if (cursor.getString(0) != null) {
												sort = String.format("%02d%03d%010d%d%s%s%s%s%03d", cursor.getInt(0),
														cursor.getInt(1), ((cursor.getInt(0) == 6) ? cursor.getLong(2)
																: 0), ((cursor.getString(3) != null) ? cursor.getInt(3)
																: 0),
														((cursor.getString(4) != null) ? cursor.getString(3) : ""),
														((cursor.getString(5) != null) ? cursor.getString(4) : ""),
														((cursor.getString(6) != null) ? cursor.getString(5) : ""),
														sheetId,
														((isInteger(quality) == true) ? Integer.parseInt(quality) : 0));
											}
										}
									} catch (SQLException e) {
										e.printStackTrace();
									} finally {
										if (cursor != null) {
											cursor.close();
										}
									}
								}
								if (name == null) {
									name = sheet;
								}
								if (inRoom == true) {
									allItems.add(new Item(id, name, sort, sheet, color, hpbuff, focusbuff, sapbuff,
											stabuff, stack, quality, locked, hp, locationName, LOC_ROOM));
								}
							}

							if ((inItem == true) && (tag.equals("item"))) {
								inItem = false;
								id = null;
								name = null;
								sheet = null;
								color = null;
								hpbuff = null;
								focusbuff = null;
								sapbuff = null;
								stabuff = null;
								stack = null;
								quality = null;
								locked = null;
								hp = null;
							}

							if (xpp.getDepth() == 3) {
								if (tag.equals("room")) {
									inRoom = false;
								}
							}

							break;
						default:
							break;
						}
						eventType = xpp.next();
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					e.printStackTrace();
				}
			} else if (xmlFile.startsWith("c")) {
				try {
					fis = context.openFileInput(xmlFile);
					byte[] buffer = new byte[fis.available()];
					while (fis.read(buffer) != -1) {
						fileContent = new String(buffer);
					}
					factory = XmlPullParserFactory.newInstance();
					factory.setNamespaceAware(true);
					XmlPullParser xpp = factory.newPullParser();
					xpp.setInput(new StringReader(fileContent));
					int eventType = xpp.getEventType();
					String tag = null;

					while (eventType != XmlPullParser.END_DOCUMENT) {
						String tagName = xpp.getName();
						switch (eventType) {
						case XmlPullParser.START_TAG:
							tag = tagName;

							if (xpp.getDepth() == 3) {
								if (tag.equals("bag")) {
									inBag = true;
								} else if (tag.equals("room")) {
									inRoom = true;
								} else if (tag.equals("pets")) {
									inPets = true;
								}
							} else if ((xpp.getDepth() == 4) && (tag.equals("item"))) {
								if ((inBag == true) || (inRoom == true)) {
									setInItem = true;
								}
							} else if ((xpp.getDepth() == 4) && (inPets == true) && (tag.equals("animal"))) {
								index = xpp.getAttributeValue(null, "index");
								if (index.equals("0")) {
									inPet1 = true;
								} else if (index.equals("1")) {
									inPet2 = true;
								} else if (index.equals("2")) {
									inPet3 = true;
								} else if (index.equals("3")) {
									inPet4 = true;

								}
							} else if ((xpp.getDepth() == 5) && (inPets == true) && (tag.equals("inventory"))) {
								if (inPet1 == true) {
									inPet1inventory = true;
								} else if (inPet2 == true) {
									inPet2inventory = true;
								} else if (inPet3 == true) {
									inPet3inventory = true;
								} else if (inPet4 == true) {
									inPet4inventory = true;
								}
							} else if ((xpp.getDepth() == 6) && (tag.equals("item"))) {
								if ((inPet1inventory == true) || (inPet2inventory == true) || (inPet3inventory == true)
										|| (inPet4inventory == true)) {
									setInItem = true;
								}
							}

							if (setInItem == true) {
								inItem = true;
								itemLevel = xpp.getDepth();
								id = xpp.getAttributeValue(null, "id");
								if (id == null) {
									Log.d(TAG, "Aborting! Could not find item id.");
									inItem = false;
								}
								setInItem = false;
							}

							break;
						case XmlPullParser.TEXT:
							if ((inItem == true) && (xpp.getDepth() == itemLevel + 1)) {
								if (tag.equals("stack")) {
									stack = xpp.getText();
								} else if (tag.equals("sheet")) {
									sheet = xpp.getText();
									if ((sheet != null) && (sheet.length() > 7)) {
										sheetId = sheet.substring(0, sheet.length() - 6);
									}
								} else if (tag.equals("quality")) {
									quality = xpp.getText();
								} else if (tag.equals("locked")) {
									locked = xpp.getText();
								} else if (tag.equals("hp")) {
									hp = xpp.getText();
								}
							} else if ((inItem == true) && (xpp.getDepth() == itemLevel + 2)) {
								if (tag.equals("color")) {
									color = xpp.getText();
								} else if (tag.equals("hpbuff")) {
									hpbuff = xpp.getText();
								} else if (tag.equals("focusbuff")) {
									focusbuff = xpp.getText();
								} else if (tag.equals("sapbuff")) {
									sapbuff = xpp.getText();
								} else if (tag.equals("stabuff")) {
									stabuff = xpp.getText();
								}
							} else if ((xpp.getDepth() == 3) && (tag.equals("name"))) {
								locationName = xpp.getText();
							}
							break;
						case XmlPullParser.END_TAG:
							tag = tagName;

							if ((inItem == true) && (tag.equals("item")) && (xpp.getDepth() == itemLevel)) {
								sort = "";
								if ((sheet != null) && (sheet.length() > 7)) {

									Cursor cursor = null;
									try {
										cursor = myDbHelper.getReadableDatabase().query("words_en_item",
												new String[] { "name" }, "_id = '" + sheetId + "'", null, null, null,
												null);
										if (cursor.moveToFirst()) {
											if (cursor.getString(0) != null) {
												name = cursor.getString(0);
											}
										}
										cursor = myDbHelper.getReadableDatabase().query(
												"items",
												new String[] { "type", "item_type", "mpft", "race", "icon_main",
														"icon_back", "txt" }, "_id = '" + sheetId + "'", null, null,
												null, null);
										if (cursor.moveToFirst()) {
											if (cursor.getString(0) != null) {
												sort = String.format("%02d%03d%010d%d%s%s%s%s%03d", cursor.getInt(0),
														cursor.getInt(1), ((cursor.getInt(0) == 6) ? cursor.getLong(2)
																: 0), ((cursor.getString(3) != null) ? cursor.getInt(3)
																: 0),
														((cursor.getString(4) != null) ? cursor.getString(3) : ""),
														((cursor.getString(5) != null) ? cursor.getString(4) : ""),
														((cursor.getString(6) != null) ? cursor.getString(5) : ""),
														sheetId,
														((isInteger(quality) == true) ? Integer.parseInt(quality) : 0));
											}
										}
									} catch (SQLException e) {
										e.printStackTrace();
									} finally {
										if (cursor != null) {
											cursor.close();
										}
									}
								}
								if (name == null) {
									name = sheet;
								}
								if (inBag == true) {
									allItems.add(new Item(id, name, sort, sheet, color, hpbuff, focusbuff, sapbuff,
											stabuff, stack, quality, locked, hp, locationName, LOC_INVENTORY));
								} else if (inRoom == true) {
									allItems.add(new Item(id, name, sort, sheet, color, hpbuff, focusbuff, sapbuff,
											stabuff, stack, quality, locked, hp, locationName, LOC_ROOM));
								} else if (inPet1inventory == true) {
									allItems.add(new Item(id, name, sort, sheet, color, hpbuff, focusbuff, sapbuff,
											stabuff, stack, quality, locked, hp, locationName, LOC_PET1));
								} else if (inPet2inventory == true) {
									allItems.add(new Item(id, name, sort, sheet, color, hpbuff, focusbuff, sapbuff,
											stabuff, stack, quality, locked, hp, locationName, LOC_PET2));
								} else if (inPet3inventory == true) {
									allItems.add(new Item(id, name, sort, sheet, color, hpbuff, focusbuff, sapbuff,
											stabuff, stack, quality, locked, hp, locationName, LOC_PET3));
								} else if (inPet4inventory == true) {
									allItems.add(new Item(id, name, sort, sheet, color, hpbuff, focusbuff, sapbuff,
											stabuff, stack, quality, locked, hp, locationName, LOC_PET4));
								}
							}

							if ((inItem == true) && (tag.equals("item"))) {
								inItem = false;
								id = null;
								name = null;
								sheet = null;
								color = null;
								hpbuff = null;
								focusbuff = null;
								sapbuff = null;
								stabuff = null;
								stack = null;
								quality = null;
								locked = null;
								hp = null;
							}

							if (xpp.getDepth() == 3) {
								if (tag.equals("bag")) {
									inBag = false;
								} else if (tag.equals("room")) {
									inRoom = false;
								} else if (tag.equals("pets")) {
									inPets = false;
								}
							} else if ((xpp.getDepth() == 4) && (tag.equals("animal"))) {
								inPet1 = inPet2 = inPet3 = inPet4 = false;
							} else if ((xpp.getDepth() == 5) && (tag.equals("inventory"))) {
								inPet1inventory = inPet2inventory = inPet3inventory = inPet4inventory = false;
							}

							break;
						default:
							break;
						}
						eventType = xpp.next();
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
