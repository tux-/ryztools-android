package no.byweb.ryzom.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/**
 * Downloader.
 * 
 * The downloader will take care of downloading data, and validates it. If file
 * is valid, it will store it.
 */
public class RyztoolsXmlDownloader extends AsyncTask<String, Integer, String> {
	static final String TAG = "RyztoolsDownloader";

	final Context context;
	final String url;
	final String params;
	boolean showToast = false;

	public RyztoolsXmlDownloader(final String url, final String params, final Context context) {
		this.url = url;
		this.params = params;
		this.context = context;
	}

	public RyztoolsXmlDownloader(final String url, final String params, final Context context, final boolean showToast) {
		this.url = url;
		this.params = params;
		this.context = context;
		this.showToast = showToast;
	}

	@Override
	protected String doInBackground(String... params) {
		String URL = RyztoolsApp.apiBasePath + url;
		if (this.params != null) {
			URL += this.params;
		}
		Log.d(TAG, URL);
		BufferedReader in = null;
		String data = null;

		try {
			HttpClient client = new DefaultHttpClient();
			URI webSite = new URI(URL);
			HttpGet request = new HttpGet();
			request.setURI(webSite);
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String l = "";
			String nl = System.getProperty("line.separator");
			while ((l = in.readLine()) != null) {
				sb.append(l + nl);
			}
			in.close();
			data = sb.toString();
			return data;
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
					return data;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		if (result != null) {
			Log.d(TAG, "Some data was downloaded.");
			boolean res = RyztoolsApp.validateXml(result, url);
			if (res != false) {
				Log.d(TAG, "The xml was accepted, continuing…");
				if (url.equals(RyztoolsApp.URL_TIME)) {
					if (FileHandler.saveFileInternally("time.xml", result.getBytes(), context)) {
						if (showToast) {
							Toast.makeText(context, "The file (time.xml) was saved.", Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(context, "The file (time.xml) could not be saved.", Toast.LENGTH_LONG)
									.show();
						}
					}
				} else if ((url.equals(RyztoolsApp.URL_CHARACTER)) || (url.equals(RyztoolsApp.URL_GUILD))) {
					if (FileHandler.saveFileInternally(params + ".xml", result.getBytes(), context)) {
						if (showToast) {
							Toast.makeText(context, "The file (" + params + ".xml) was saved.", Toast.LENGTH_LONG)
									.show();
						} else {
							Toast.makeText(context, "The file (" + params + ".xml) could not be saved.",
									Toast.LENGTH_LONG).show();
						}
					}
				}
			} else {
				if (showToast) {
					Toast.makeText(context, "Xml was not accepted, not saving.", Toast.LENGTH_LONG).show();
				}
				Log.d(TAG, "Xml was not accepted, not saving.");
			}

		} else {
			if (showToast) {
				Toast.makeText(context, "Somehow the download failed.", Toast.LENGTH_LONG).show();
			}
			Log.d(TAG, "Somehow the download failed.");
		}
	}

}
