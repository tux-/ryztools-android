package no.byweb.ryzom.guild;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.byweb.ryzom.dev.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GuildsActivity extends Activity {
	static final String TAG = "GuildsActivity";

	ArrayList<Key> keys = new ArrayList<Key>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guilds);

		PopulateList updater = new PopulateList();
		updater.execute();
	}

	private class PopulateList extends AsyncTask<String, Integer, List<Key>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected List<Key> doInBackground(String... arg0) {
			List<Key> keys = new ArrayList<Key>();
			String[] SavedFiles = getApplicationContext().fileList();
			String name;
			String apiModules;
			XmlPullParserFactory factory;
			FileInputStream fis = null;
			String fileContent = null;

			for (int i = 0; i < SavedFiles.length; i++) {
				if ((SavedFiles[i].length() == 45) && (SavedFiles[i].startsWith("g"))) {
					name = "Unknown";
					apiModules = "Unknown";

					try {
						fis = getApplicationContext().openFileInput(SavedFiles[i]);
						byte[] buffer = new byte[fis.available()];
						while (fis.read(buffer) != -1) {
							fileContent = new String(buffer);
						}
						factory = XmlPullParserFactory.newInstance();
						factory.setNamespaceAware(true);
						XmlPullParser xpp = factory.newPullParser();
						xpp.setInput(new StringReader(fileContent));
						int eventType = xpp.getEventType();
						String tag = null;
						while (eventType != XmlPullParser.END_DOCUMENT) {
							String tagName = xpp.getName();
							switch (eventType) {
							case XmlPullParser.START_TAG:
								tag = tagName;
								if ((xpp.getDepth() == 2) && (tag.equals("guild"))) {
									if (xpp.getAttributeValue(null, "modules") != null) {
										apiModules = xpp.getAttributeValue(null, "modules");
									}
								}
								break;
							case XmlPullParser.TEXT:
								if ((xpp.getDepth() == 3) && (tag.equals("name"))) {
									name = xpp.getText();
								}
								break;
							case XmlPullParser.END_TAG:
								tag = tagName;
								break;
							default:
								break;
							}
							eventType = xpp.next();
						}
					} catch (XmlPullParserException e) {
						e.printStackTrace();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					keys.add(new Key(name, apiModules, SavedFiles[i]));
				}
			}
			return keys;
		}

		@Override
		protected void onPostExecute(List<Key> result) {
			keys.clear();
			for (int i = 0; i < result.size(); i++) {
				keys.add(result.get(i));
			}
			boolean res = populateListView();
			if (res) {
				registerClickCallback();
			}
		}
	}

	private boolean populateListView() {
		boolean res = true;
		ArrayAdapter<Key> adapter = null;
		try {
			adapter = new MyListAdapter(this);

			adapter.notifyDataSetChanged();
			ListView list = (ListView) findViewById(R.id.guilds);
			list.setEmptyView(findViewById(R.id.txtEmptyList));

			list.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
			res = false;
		}
		return res;
	}

	private class MyListAdapter extends ArrayAdapter<Key> {
		public MyListAdapter(Context context) {
			super(context, R.layout.list_item_guild, keys);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Make sure we have a view to work with (may have been given a null)
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater().inflate(R.layout.list_item_guild, parent, false);
			}

			Collections.sort(keys, new CustomComparator());
			Key currentKey = keys.get(position);

			itemView.setTag(currentKey.getId());

			TextView nameText = (TextView) itemView.findViewById(R.id.item_txtGuildName);
			nameText.setText(currentKey.getName());

			if (currentKey.getApiModules() != null) {
				TextView apiModulesText = (TextView) itemView.findViewById(R.id.item_txtApiModules);
				apiModulesText.setText("API Modules: " + currentKey.getApiModules());
			}

			return itemView;
		}

	}

	private void registerClickCallback() {
		ListView list = (ListView) findViewById(R.id.guilds);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
				final String clickedKey = viewClicked.getTag().toString();

				Intent intent = new Intent(GuildsActivity.this, GuildActivity.class);
				intent.putExtra("XML_FILE", clickedKey);
				startActivity(intent);
			}
		});
	}

	private class CustomComparator implements Comparator<Key> {
		@Override
		public int compare(Key o1, Key o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	private class Key {
		private String name;
		private String apiModules;
		private String id;

		public Key(String name, String apiModules, String id) {
			super();
			this.name = name;
			this.apiModules = apiModules;
			this.id = id;
		}

		public String getName() {
			return this.name;
		}

		public String getApiModules() {
			return this.apiModules;
		}

		public String getId() {
			return this.id;
		}

	}

}
