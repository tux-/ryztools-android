package no.byweb.ryzom.preferences;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import no.byweb.ryzom.dev.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PreferencesApiKeysFragment extends Fragment {
	static final String TAG = "PreferencesApiKeysFragment";

	private List<Key> keys = new ArrayList<Key>();

	private boolean running = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.preferences_api_keys, container, false);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		running = true;
		new Thread() {

			@Override
			public void run() {
				while (running) {
					PopulateList updater = new PopulateList();
					updater.execute();
					try {
						Thread.sleep(2 * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	@Override
	public void onPause() {
		super.onPause();
		running = false;
	}

	private class PopulateList extends AsyncTask<String, Integer, List<Key>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected List<Key> doInBackground(String... arg0) {
			List<Key> keys = new ArrayList<Key>();
			String[] SavedFiles = getActivity().getApplicationContext().fileList();
			String name;
			String type;
			String lastUpdate;
			String apiModules;
			long created;
			XmlPullParserFactory factory;
			FileInputStream fis = null;
			String fileContent = null;

			for (int i = 0; i < SavedFiles.length; i++) {
				if (SavedFiles[i].length() == 45) {
					name = "Unknown";
					apiModules = "Unknown";
					created = 0;
					if (SavedFiles[i].startsWith("c")) {
						type = "Character";
					} else {
						type = "Guild";
					}
					try {
						fis = getActivity().getApplicationContext().openFileInput(SavedFiles[i]);
						byte[] buffer = new byte[fis.available()];
						while (fis.read(buffer) != -1) {
							fileContent = new String(buffer);
						}
						factory = XmlPullParserFactory.newInstance();
						factory.setNamespaceAware(true);
						XmlPullParser xpp = factory.newPullParser();
						xpp.setInput(new StringReader(fileContent));
						int eventType = xpp.getEventType();
						String tag = null;
						while (eventType != XmlPullParser.END_DOCUMENT) {
							String tagName = xpp.getName();
							switch (eventType) {
							case XmlPullParser.START_TAG:
								tag = tagName;
								if ((xpp.getDepth() == 2) && ((tag.equals("character")) || (tag.equals("guild")))) {
									if (xpp.getAttributeValue(null, "created") != null) {
										created = Long.valueOf(xpp.getAttributeValue(null, "created"));
									}
									if (xpp.getAttributeValue(null, "modules") != null) {
										apiModules = xpp.getAttributeValue(null, "modules");
									}
								}
								break;
							case XmlPullParser.TEXT:
								if ((xpp.getDepth() == 3) && (tag.equals("name"))) {
									name = xpp.getText();
								}
								break;
							case XmlPullParser.END_TAG:
								tag = tagName;
								break;
							default:
								break;
							}
							eventType = xpp.next();
						}
					} catch (XmlPullParserException e) {
						e.printStackTrace();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (created > 0) {
						CharSequence relativeTimeSpan = DateUtils.getRelativeTimeSpanString(created * 1000);
						lastUpdate = relativeTimeSpan.toString();
					} else {
						lastUpdate = "Unknown";
					}
					keys.add(new Key(name, type, lastUpdate, apiModules, SavedFiles[i]));
				}
			}
			return keys;
		}

		@Override
		protected void onPostExecute(List<Key> result) {
			keys.clear();
			for (int i = 0; i < result.size(); i++) {
				keys.add(result.get(i));
			}
			boolean res = populateListView();
			if (res) {
				registerClickCallback();
			}
		}
	}

	private boolean populateListView() {
		boolean res = true;
		ArrayAdapter<Key> adapter = null;
		try {
			adapter = new MyListAdapter(getActivity());

			adapter.notifyDataSetChanged();
			ListView list = (ListView) getView().findViewById(R.id.keysListView);
			list.setEmptyView(getView().findViewById(R.id.empty_list_item));

			list.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
			res = false;
		}
		return res;
	}

	private class MyListAdapter extends ArrayAdapter<Key> {
		public MyListAdapter(Context context) {
			super(context, R.layout.list_item_preferences_api_key, keys);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Make sure we have a view to work with (may have been given a null)
			View itemView = convertView;
			if (itemView == null) {
				itemView = getActivity().getLayoutInflater().inflate(R.layout.list_item_preferences_api_key, parent,
						false);
			}

			Key currentKey = keys.get(position);

			itemView.setTag(currentKey.getId());

			TextView nameText = (TextView) itemView.findViewById(R.id.item_textName);
			nameText.setText(currentKey.getName());

			TextView typeText = (TextView) itemView.findViewById(R.id.item_textType);
			typeText.setText(currentKey.getType());

			TextView lastUpdateText = (TextView) itemView.findViewById(R.id.item_textLastUpdate);
			lastUpdateText.setText(getResources().getString(R.string.Last_updated) + ": " + currentKey.getLastUpdate());

			return itemView;
		}

	}

	private void registerClickCallback() {
		ListView list = (ListView) getActivity().findViewById(R.id.keysListView);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
				final String clickedKey = viewClicked.getTag().toString();
				Key currentKey = keys.get(position);
				Log.d(TAG, currentKey.getName());

				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

				builder.setTitle(R.string.dialog_add_key_title);

				builder.setMessage("Do you want to delete " + currentKey.getName() + " key with modules: "
						+ currentKey.getApiModules() + "?");

				builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User clicked OK button

						getActivity().deleteFile(clickedKey);
					}
				});
				builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog

						dialog.dismiss();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});

	}

	private class Key {
		private String name;
		private String type;
		private String lastUpdate;
		private String apiModules;
		private String id;

		public Key(String name, String type, String lastUpdate, String apiModules, String id) {
			super();
			this.name = name;
			this.type = type;
			this.lastUpdate = lastUpdate;
			this.apiModules = apiModules;
			this.id = id;
		}

		public String getName() {
			return this.name;
		}

		public String getType() {
			return this.type;
		}

		public String getLastUpdate() {
			return this.lastUpdate;
		}

		public String getApiModules() {
			return this.apiModules;
		}

		public String getId() {
			return this.id;
		}

	}

}
